'use strict';

import React from 'react';
import withAuth from  './Login/utils/withAuth'

class IndexPage extends React.Component {
  render() {
    return (
      <div id="HomeView">
        <Prestataire/>
        <Calendar />
      </div>

    );
  }
}

class Calendar extends React.Component {
  render() {
    return <div className="col-lg-10 col-md-12" id="the_calendar"></div>;
  }
  componentDidMount() {
    $('#the_calendar').fullCalendar({
      header: {
        left: 'prev, agendaWeek',
        center: 'today',
        right: 'agendaDay,next'
      },
      // events: function(start, end, timezone, callback) {
      //   $.ajax({
      //     url: 'http://localhost:3001/agenda',
      //     dataType: 'JSON',
      //     data: {
      //       // our hypothetical feed requires UNIX timestamps
      //       start: start.unix(),
      //       end: end.unix()
      //     },
      //     success: function(response) {
      //
      //       var events = [];
      //       for(var i= 0; i<response.length; i++)
      //       {
      //         events.push({
      //           title: "Coupe de cheveux",
      //           start: moment(response[i].started_date).format('YYYY-MM-DDTHH:mm:ssZ'),
      //           end: moment(response[i].ended_date).format('YYYY-MM-DDTHH:mm:ssZ'),
      //         });
      //
      //       }
      //
      //       console.log(events);
      //       callback(events);
      //     }
      //   });
      // },
      firstDay: 1,
      slotEventOverlap:false,
      defaultView : 'agendaWeek',
      hiddenDays: [ 0 ],
      businessHours: {
        // days of week. an array of zero-based day of week integers (0=Sunday)
        dow: [ 1, 2, 3, 4, 5 ,6  ], // Monday - Thursday

        start: '8:00', // a start time (10am in this example)
        end: '19:00', // an end time (6pm in this example)
      },
      minTime: '8:00',
      maxTime: '19:00',
      allDaySlot:false,
      height: 565,
      lang:'fr',
    })
  }
}

class Prestataire extends React.Component {
  searchPresta(event){
    $('.item_presta').each(function(){
      $(this).attr('data-search-term', $(this).text().toLowerCase());
    });

    var that = event.target.value.toLowerCase();

    $('.item_presta').each(function(){
      if ($(this).filter('[data-search-term *= ' + that + ']').length > 0 || that.length < 1) {
        $(this).show();
      } else {
        $(this).hide();
      }

    });

  }

  saySomething(something,az) {
    $('li').removeClass("active");
    $(something).addClass("active");
    console.log(az);


    var event={id:1 , title: az+' rendez-vous', start:  "2017-03-22T11:30:00"};
    $('#the_calendar').fullCalendar( 'renderEvent', event, true);
  }

  handleClick(e) {
    this.saySomething(e.target,$(e.target).html());
  }
  render() {
    return (
      <div id="prestaList" className="no-padding-left col-lg-2 col-md-12">
        <input type="text" className="hidden-xs hidden-sm searchPresta" onKeyUp={this.searchPresta} placeholder="Rechercher dans la liste"/>
        <ul className="visible-lg">
          <li className="active item_presta" onClick={this.handleClick.bind(this)}>Anais</li>
          <li className="item_presta" onClick={this.handleClick.bind(this)}>Julie</li>
          <li className="item_presta" onClick={this.handleClick.bind(this)}>Emilie</li>
          <li className="item_presta" onClick={this.handleClick.bind(this)}>Julien</li>
          <li className="item_presta" onClick={this.handleClick.bind(this)}>Anne</li>
          <li className="item_presta" onClick={this.handleClick.bind(this)}>Laurence</li>
          <li className="item_presta" onClick={this.handleClick.bind(this)}>Lorie</li>
          <li className="item_presta" onClick={this.handleClick.bind(this)}>Jeanne</li>
          <li className="item_presta" onClick={this.handleClick.bind(this)}>Heloise</li>
          <li className="item_presta" onClick={this.handleClick.bind(this)}>Pierre</li>
          <li className="item_presta" onClick={this.handleClick.bind(this)}>Julien</li>
          <li className="item_presta" onClick={this.handleClick.bind(this)}>Anne</li>
        </ul>
      </div>
    );
  }
}

export default withAuth(IndexPage)
