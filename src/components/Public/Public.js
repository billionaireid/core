/**
 * Created by antoine on 31/03/2017.
 */
import React, {Component} from "react";
import AuthService from "../Login/utils/AuthService";
var Router = require('react-router');

const auth = new AuthService()

export default class Public extends Component {
  render() {
    return (
      <div className="AppPublic">
        <div className="vertical_align">
        <div className="form_sign_in  center_block">
          <div className="col-md-6 col-xs-12">
              <h2 className="form-signin-heading text-center">ESPACE PROFESSIONNEL</h2>
            <h5 className="text-center"> Connectez vous à votre espace de gestion </h5>
          </div>
          <div className="col-md-5 col-xs-12 ">
                <PublicSite />
          </div>
        </div>
        </div>
        <h6 className="byLove"> Powered by Nous </h6>
      </div>
    );
  }
}

class PublicSite extends Component {
  constructor(props) {
    super(props)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  componentDidMount () {
    if (auth.loggedIn()) {
      console.log(auth.loggedIn());
      console.log(auth.getProfile())
      Router.browserHistory.push('/pro');   // redirect if you're already logged in
    }
  }

  handleSubmit (e) {
    e.preventDefault()

    auth.login(this.refs.email.value, this.refs.password.value)

  }

  noMessage(){
    $('.error').addClass('hidden');
  }

  render() {
    return (

        <form className="form-signin" onSubmit={this.handleSubmit} onKeyDown={this.noMessage} >
          <input type="email" ref="email" id="inputEmail" className="form-control" placeholder="Adresse mail" required="" autoFocus="" />
          <input type="password" ref="password" id="inputPassword" className="form-control" placeholder="Mot de passe" required="" />

          <h4 className="error hidden"> Erreur lors de votre authentification </h4>

          <div className="text-center">
            <button className="btn btn-primary text-right btn-block" type="submit">Entrer</button>
          </div>
        </form>
    )
  }
}
