import React, {Component} from 'react'
var Router = require('react-router');
import AuthService from './utils/AuthService'

const auth = new AuthService()

class Login extends Component {
  constructor(props) {
    super(props)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  componentDidMount () {
    if (auth.loggedIn()) {
      console.log(auth.loggedIn());
      console.log(auth.getProfile())
      Router.browserHistory.push('/pro');   // redirect if you're already logged in
    }
  }

  handleSubmit (e) {
    e.preventDefault()
    // yay uncontrolled forms!
    auth.login(this.refs.email.value, this.refs.password.value)
      .then(res => {
        console.log(res)
        Router.browserHistory.push('/pro');
      })
      .catch(e => console.log(e))  // you would show/hide error messages with component state here
  }

  render () {
    return (
      <div>
        Login
        <form onSubmit={this.handleSubmit} >
          <input type="text" ref="email"/>
          <input type="password" ref="password"/>
          <input type="submit" value="Submit"/>
        </form>
      </div>
    )
  }
}

export default Login
