var Router = require('react-router');

export default class AuthService {
  constructor() {
    this.domain = 'http://localhost:3000'
    this.fetch = this.fetch.bind(this)
    this.login = this.login.bind(this)
    this.getProfile = this.getProfile.bind(this)
  }

  login(email, password) {
    var that = this;

    if(!email && !password){

    }else
    {
      $.ajax({
        url: "http://localhost:3001/users/login",
        dataType: "json",
        type: 'POST', //I want a type as POST
        data: {password:password,
          email:email},
        success: function(data){
          that.setToken("AZIUGEAZEABZEAZO999");
          that.setProfile({mail:email,name:data[0].first_name});
          Router.browserHistory.push('/pro');
        },error: function(data){
          console.log(data);
          $('.error').removeClass('hidden');
        }
      });

    }
    return Promise.resolve(this.getProfile())
    }

  loggedIn(){
    // Checks if there is a saved token and it's still valid
    const token = this.getToken()
    return !!token // handwaiving here
  }

  setProfile(profile){
    // Saves profile data to localStorage
    localStorage.setItem('profile', JSON.stringify(profile))
  }

  getProfile(){
    // Retrieves the profile data from localStorage
    const profile = localStorage.getItem('profile')
    return profile ? JSON.parse(localStorage.profile) : {}
  }

  setToken(idToken){
    // Saves user token to localStorage
    localStorage.setItem('id_token', idToken)
  }

  getToken(){
    // Retrieves the user token from localStorage
    return localStorage.getItem('id_token')
  }

  logout(){
    // Clear user token and profile data from localStorage
    localStorage.removeItem('id_token');
    localStorage.removeItem('profile');
  }

  _checkStatus(response) {
    // raises an error in case response status is not a success
    if (response.status >= 200 && response.status < 300) {
      return response
    } else {
      var error = new Error(response.statusText)
      error.response = response
      throw error
    }
  }

  fetch(url, options){
    // performs api calls sending the required authentication headers
    const headers = {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }

    if (this.loggedIn()){
      headers['Authorization'] = 'Bearer ' + this.getToken()
    }

    return fetch(url, {
      headers,
    })
      .then(this._checkStatus)
      .then(response => response.json())
  }
}
