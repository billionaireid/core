'use strict';

import React from 'react';
import { Link } from 'react-router';
import { IndexLink } from 'react-router';
import withAuth from  '../Login/utils/withAuth'
import AuthService from '../Login/utils/AuthService'
var Router = require('react-router');

const auth = new AuthService()

class Layout extends React.Component {
  constructor(props) {
    super(props)
    this.handleLogout = this.handleLogout.bind(this)
  }

  componentDidMount () {

  }

  handleLogout (e) {
    e.preventDefault()
    // yay uncontrolled forms!
    auth.logout()
    Router.browserHistory.push('/');
  }

  render() {
    const user = this.props.auth.getProfile();
    return (
      <div className='App'>
        <nav className="navbar navbar-default navbar-static-top">
          <div className="container">
            <div className="navbar-header">
              <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span className="sr-only">Toggle navigation</span>
                <span className="icon-bar"></span>
                <span className="icon-bar"></span>
                <span className="icon-bar"></span>
              </button>
              <a className="navbar-brand" href="#">Elisa Coif'</a>
            </div>
            <div id="navbar" className="navbar-collapse collapse">
              <ul className="nav navbar-nav navbar-left">
                <li><IndexLink activeClassName='act' to='/pro'>Calendrier</IndexLink></li>
                <li><Link activeClassName='act' to='/pro/clients'>Clients</Link></li>
              </ul>
              <ul className="nav navbar-nav navbar-right">
                <li><a href="#">{user.name}</a></li>
                <li className="nav_border_left" onClick={this.handleLogout}><a href=""> Déconnexion</a></li>
              </ul>
            </div>
          </div>
        </nav>
        <div className="col-md-12 col-xs-12">
          { this.props.children }
        </div>
        <div className="modal fade" id="sign-out-modal">
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <h4 className="modal-title">Déconnexion</h4>
              </div>
              <div className="modal-body">
                Vous allez être déconnecté.
              </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-default" data-dismiss="modal">Annuler</button>
                <button type="button" className="btn btn-primary">Confirmer</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default withAuth(Layout);
