'use strict';

import React from 'react'
import { Route, IndexRoute, Router, browserHistory } from 'react-router'


import Layout from './components/Layout/Layout';
import Public from './components/Public/Public';
import IndexPage from './components/IndexPage';
import Clients from './components/Clients/Clients';
import NotFoundPage from './components/NotFoundPage/NotFoundPage';


const routes = (
  <Router history={browserHistory}>
    <Route path="/" component={Public} />
    <Route path="/pro"  component={Layout}>
      <IndexRoute component={IndexPage}/>
      <Route path="clients" component={Clients}/>
    </Route>
    <Route path="*" component={NotFoundPage}/>
  </Router>
);

export default routes;
