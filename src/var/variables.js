/**
 * Created by antoine on 31/03/2017.
 */
var Variables = module.exports = {
  auth : false,
  setAuthTrue: function() {
    Variables.auth = true;
  },
  setAuthFalse: function() {
    Variables.auth = false;
  }
}
