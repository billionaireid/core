# Core project for agenda #

##  3 Branches  

* Master -> Production
* Dev -> Developpement
* ASM -> Test

## Run the project 

### Dev 

```
#!bash

npm run start-dev-hmr
```


### Production
*First compile* 

 
```
#!bash

   npm run build
```
*Then*


```
#!bash

pm2 start npm --name "Calendar" -- -a run start
```